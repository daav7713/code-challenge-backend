const express = require('express');
const admin = require('firebase-admin');
const cors = require('cors');
const app = express();

const serviceAccount = require('./pwa-code-challenge-bbba3-firebase-adminsdk-lz4v6-5fefdd89f5.json');
// Initialize Firebase Admin with your server credentials
admin.initializeApp({
    credential: admin.credential.cert(serviceAccount)
});

app.use(cors({
    origin: '*'
}));

app.use(express.json());

app.post('/send-notification', (req, res) => {
  const message = {
    notification: {
      title: req?.body?.title || 'Notification title',
      body: req?.body?.body || 'Notification body',
    },
    token: req?.body?.token
  };

  admin.messaging().send(message)
    .then(response => res.status(200).send(response))
    .catch(error => res.status(500).send(error));
});

const PORT = process.env.PORT || 3000;
app.listen(PORT, () => console.log(`Server running on port ${PORT}`));